; Создать функцию find_word. Она принимает два аргумента:
; Указатель на нуль-терминированную строку.
; Указатель на начало словаря.
; find_word пройдёт по всему словарю в поисках подходящего ключа.
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в
; словарь (не значения), иначе вернёт 0.

%define size 8

extern string_equals
global find_word



find_word:
	xor rax, rax

	.loop:
		cmp rsi, 0 ; rsi - начало словаря → проверка, закончился ли словарь
		je .error
		push rdi ; сохранение регистров перед string_equals
		push rsi
		add rsi, size ; указатель - на следующий элемент
		call string_equals
		pop rsi 
		pop rdi
		cmp rax, 1  ; проверка возвращённого результата string_equals
		je .finish
		mov rax, [rsi]
		cmp rax, 0
		je .error
		mov rsi, [rsi] ; переход к следующему элементу
		jmp .loop

    .finish:
		mov rax, rsi
		ret
		
	.error:
		xor rax, rax
		ret
		
