%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%define BUFFER 256

global _start
extern find_word



section .data

err_msg1: db "Некорректная строка на входе", 0
err_msg2: db "Слово не найдено", 0
buffer: times BUFFER db 0 



; Читает строку размером не более 255 символов в буфер с stdin.
; Пытается найти вхождение в словаре; если оно найдено,
; распечатывает в stdout значение по этому ключу.
; Иначе выдает сообщение об ошибке.
; Сообщения об ошибках нужно выводить в stderr.

section .text

_start:
    mov rsi, BUFFER ; длина буфера и указатель
    mov rdi, buffer ; параметры read_word
    call read_word

    cmp rax, 0 ; 0 возникает при ошибке чтения, проверка на 0
    je .readerr

    mov rdi, rax ; указатель, возвращённый из read_word
    mov rsi, dict_start ; адрес начала словаря
    call find_word 

    cmp rax, 0 ; 0 возникает при ошибке чтения, проверка на 0
    je .notfounderr
    add rax, 8 ; взятие следующего элемента
    mov rdi, rax
    call string_length

    add rdi, rax ; пропуск длины слова
    add rdi, 1 ; в rdi - адрес значения
    call print_string
    jmp .finish

    .readerr:
	    mov rdi, err_msg1
	    call print_error
	    call exit

    .notfounderr:
	    mov rdi, err_msg2
	    call print_error
	    call exit

    .finish:
        call print_newline
	    call exit
