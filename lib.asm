section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall



; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop

    .end:
        ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax 
    mov rax, 1 
    mov rsi, rdi 
    mov rdi, 1 
    syscall
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA ; 



; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi ; сохраняем символ в стеке
    mov rsi, rsp ; передача строки со стека
    mov rax, 1
    mov rdx, 1 ; количество символов (один)
    mov rdi, 1
    syscall
    pop rdi
    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r8, 10 ; делитель
    mov rax, rdi ; передача числа в аккумулятор
    mov r9, rsp 
    sub rsp, 1

    .loop:
        xor rdx, rdx 
        div r8 
        add dl, '0' 
        sub rsp, 1
        mov [rsp], dl 
        cmp rax, 0
        jne .loop
        mov rdi, rsp
        call print_string
        mov rsp, r9 ; восстанавливаем rsp
        ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .unsigned_number
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .unsigned_number:   
        call print_uint
        ret 



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx

    .loop:
        mov cl, byte[rdi+rax]
	    cmp byte[rsi+rax], cl ; сравниваем байты
	    jne .not_eq
	    cmp byte[rdi+rax], 0 
	    je .eq
	    inc rax 
	    jmp .loop

    .not_eq:
	    mov rax, 0
	    ret

    .eq:
	    mov rax, 1
	    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	push rax
	mov rdi, 0
	mov rdx, 1
	mov rsi, rsp
	syscall
	pop rax
	ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    xor rdx, rdx
	xor rcx, rcx

	.skip:
		dec rsi
		cmp rsi, 0
		je .end
		push rdi
		push rdx
		push rsi
		call read_char
		pop rsi
		pop rdx
		pop rdi
		cmp al, 0x20
		je .skip
		cmp al, 0x9
		je .skip
		cmp al, 0xA
		je .skip

	.read:
		cmp al, 0
		je .success
		mov [rdi+rdx], rax
		inc rdx
		dec rsi
		cmp rsi, 0
		je .end
		push rdi
		push rdx
		push rsi
		call read_char
		pop rsi
		pop rdx
		pop rdi
		cmp al, 0x20
		je .success
		cmp al, 0x9
		je .success
		cmp al, 0xA
		je .success
		jmp .read

	.end:
		mov rax, 0
		ret

	.success:
		mov rax, rdi
		ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8

    .loop:
        cmp byte[rdi + rdx], '0' ; проверка на "меньше нуля"
        jb .finish
        cmp byte[rdi + rdx], '9' ; проверка на "больше девяти"
	    ja .finish

        inc r8 ; инкремент счётчика прочитанных цифр

        imul rax, 10 ; для взятия следующей цифры
        add al, byte[rdi] ; прибавление этой цифры
        sub al, '0'
		inc rdi ; инкремент счётчика длины
        jmp .loop

    .finish:
        mov rdx, r8
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte[rdi], '-' ; проверка знака числа
	jne .unsigned_number
	inc rdi ; переход к следующей цифре
	call parse_uint
	neg rax ; изменение знака
	inc rdx ; инкремент счётчика
	ret

    .unsigned_number:
        call parse_uint	
        ret 



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    xor r9, r9
    cmp rax, rdx ; сравнивание длины строки с длиной буфера
	je .error

    .loop:
	    cmp rax, rdx ; сравнивание длины строки с длиной буфера
	    je .error
        mov cl, byte[rdi + rax] 
	    mov byte[rsi + rax], cl ; копирование символа в буфер
	    cmp byte[rdi + rax], 0
	    je .end
	    inc rax ; увеличение длины строки
	    jmp .loop

    .error:
	    mov rax, 0
	    ret

    .end:
	    ret

print_error:
    call string_length
    mov     rdx, rax
    mov     rsi, rdi
    mov     rax, 1
    mov     rdi, 2 ; дескриптор stderr 
    syscall
    ret
